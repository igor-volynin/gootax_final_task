<?php

namespace app\models;

use Yii;
use app\modules\review\models\Review;
use app\modules\city\models\City;

/**
 * This is the model class for table "city_review".
 *
 * @property int $id
 * @property int $id_city
 * @property int $id_review
 *
 * @property City $city
 * @property Review $review
 */
class CityReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_city', 'id_review'], 'integer'],
            [['id_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['id_city' => 'id']],
            [['id_review'], 'exist', 'skipOnError' => true, 'targetClass' => Review::className(), 'targetAttribute' => ['id_review' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_city' => 'Id City',
            'id_review' => 'Id Review',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'id_city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Review::className(), ['id' => 'id_review']);
    }
    
    public function pairExist($id_city, $id_review)
    {
        if (CityReview::find()->where(['id_review' => $id_review])->andWhere(['id_city' => $id_city])->one()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function createNewPair($id_city, $id_review)
    {
        $pair = new CityReview;
        $pair->id_city = $id_city;
        $pair->id_review = $id_review;
        $pair->save();
    }
}

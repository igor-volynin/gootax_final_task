<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CityName extends Model
{
    public $name;
    
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => '',
        ];
    }
}
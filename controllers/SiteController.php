<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\CityName;
use app\modules\city\models\City;
use app\modules\review\models\Review;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(Yii::$app->session->has('city')) {
            $city = City::find()->where(['name' => Yii::$app->session->get('city')])->one();
            $reviews = $city->getCityReviews()->with('review');
            $dataProvider = new ActiveDataProvider([
                'query' => $reviews,
                'pagination' => [
                    'pageSize' => 5,
                ]
            ]);
            return $this->render('review_list', [
                'dataProvider' => $dataProvider,
                ]);         
        }
        
        $geo = new \jisoft\sypexgeo\Sypexgeo();
        $geo->get($_SERVER['REMOTE_ADDR']);
        
        if (isset($geo->city['name_ru'])) {
            $city = City::find()->where(['name' => $geo->city['name_ru']])->one();
            if (isset($city->name)) {
                $model = new CityName;
                $model->name = $city->name;
                if ($model->load(Yii::$app->request->post())) {
                    Yii::$app->session->set('city', $model->name);
                    $this->refresh();
                }
                return $this->render('index', ['model' => $model]);
            } else {
                return $this->redirect('/city/default/choose');
            }
        } else {
            return $this->redirect('/city/default/choose');
        }
    }
}

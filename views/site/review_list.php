<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\review\models\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отзывы';
?>
<div class="site-review_list">

    <h3><?= Html::encode(Yii::$app->session->get('city')) ?></h3>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
	    'summary' => false,
    ]); ?>
    
</div>
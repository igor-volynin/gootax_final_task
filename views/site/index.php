<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
<?php if( Yii::$app->session->hasFlash('send_mail_message') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('send_mail_message'); ?>
    </div>
<?php endif;?>
<?php if( Yii::$app->session->hasFlash('login_is_active') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('login_is_active'); ?>
    </div>
<?php endif;?>
<div class="site-index">
    <div class="body-content">
        <div class="jumbotron">
            <h2>Здравствуйте!</h2>

            <p class="lead">Ваш город <?= $model->name ?>?</p>
            
            <div class="review-form">
            <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($model, 'name')->hiddenInput()->label(false) ?>

            <div class="form-group">
                <?= Html::submitButton('Да', ['class' => 'btn btn-success', 'style' => 'width: 120px']) ?>
                <?= Html::a('Другой', ['/city/default/choose'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

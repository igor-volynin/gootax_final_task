<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181029_115852_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string(),
            'date_create' => $this->integer()->notNull(),
            'password' => $this->string()->notNull(),
            'activation_code' => $this->string(),
            'is_active' => $this->integer()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

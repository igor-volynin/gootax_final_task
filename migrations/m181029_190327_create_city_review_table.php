<?php

use yii\db\Migration;

/**
 * Handles the creation of table `city_review`.
 */
class m181029_190327_create_city_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('city_review', [
            'id' => $this->primaryKey(),
            'id_city' => $this->integer(),
            'id_review' => $this->integer(),
        ]);
    
        $this->createIndex(
            'idx-id_city',
            'city_review',
            'id_city'
        );
        
        $this->addForeignKey(
            'fk-id_city',
            'city_review',
            'id_city',
            'city',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-id_review',
            'city_review',
            'id_review'
        );
        
        $this->addForeignKey(
            'fk-id_review',
            'city_review',
            'id_review',
            'review',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('city_review');
    }
}

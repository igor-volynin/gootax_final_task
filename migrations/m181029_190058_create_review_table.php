<?php

use yii\db\Migration;

/**
 * Handles the creation of table `review`.
 */
class m181029_190058_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('review', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'text' => $this->text(),
            'rating' => $this->integer(),
            'img' => $this->string(),
            'id_author' => $this->integer(),
            'date_create' => $this->integer(),
        ]);
        
        $this->createIndex(
            'idx-id_author',
            'review',
            'id_author'
        );
        
        $this->addForeignKey(
            'fk-id_author',
            'review',
            'id_author',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('review');
    }
}

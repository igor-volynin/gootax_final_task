<?php

use app\modules\user\models\User;
use app\modules\city\models\City;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div>

    <div class="blog-post">
        <h4 class="blog-post-title"><?= $model->title ?> (для городов:
        <?php $city_list = City::getCityList($model);
              foreach($city_list as $city) {
                  echo $city.' ';
              }
        ?>)</h4>
        <?php for($i=0;$i<$model->rating;$i++) { echo '<span class="glyphicon glyphicon-star" style="color: gold"></span>'; } ?>
    </div>
    <?= Html::img(Yii::$app->urlManager->createUrl('img/'.$model->img)) ?>
    <p><?= Html::encode($model->text) ?></p>
    
    <?php if (Yii::$app->user->isGuest): ?>
    <p class="blog-post-meta"><?= Html::encode(User::find()->where(['id' => $model->id_author])->one()->fio) ?></p>
    <?php else: ?>
    <a href="" data-toggle="modal" data-target="#myModal"><?= Html::encode(User::find()->where(['id' => $model->id_author])->one()->fio) ?></a>
    <a href="/review/update?id=<?= $model->id ?>"><span class="glyphicon glyphicon-edit"></span></a>
    <?php endif;?>
    <p><?= date('d.m.Y',$model->date_create) ?></p>
    <hr>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?= Html::encode(User::find()->where(['id' => $model->id_author])->one()->fio) ?></h4>
      </div>
      <div class="modal-body">
        <p>Email: <?= Html::encode(User::find()->where(['id' => $model->id_author])->one()->email) ?></p>
        <p>Телефон: <?= Html::encode(User::find()->where(['id' => $model->id_author])->one()->phone) ?></p>
        <a href="/review/user?id=<?= User::find()->where(['id' => $model->id_author])->one()->id ?>" >Все отзывы</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div>
  </div>
</div>

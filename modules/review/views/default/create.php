<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\review\models\Review */

$this->title = 'Добавить отзыв';
?>
<div class="review-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'cities' => $cities,
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\select2\Select2Bootstrap;

/* @var $this yii\web\View */
/* @var $model app\modules\review\models\Review */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="review-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'rating')->dropDownList([
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        ],
        ['class' => 'dropdown']); ?>
    
    <?= $form->field($model, 'city')->widget(
        Select2Bootstrap::class, 
        [
            'options' => ['class' => "js-example-basic-multiple",'multiple' => "multiple", 'style' => 'width: 100%'],
            'items' => $cities, // $data should be the same as the items provided to a regular yii2 dropdownlist
            'template' => '{input}'
        ]
    );?>
    <?= $form->field($model, 'other_city')->textInput() ?>
    <?= $form->field($model, 'image_file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

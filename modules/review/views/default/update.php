<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\review\models\Review */

$this->title = 'Редактировать отзыв: ' . $model->title;
?>
<div class="review-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>

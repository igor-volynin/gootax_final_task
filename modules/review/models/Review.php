<?php

namespace app\modules\review\models;

use Yii;
use app\modules\user\models\User;
use app\modules\city\models\City;
use app\models\CityReview;
use yii\web\UploadedFile;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property int $rating
 * @property string $img
 * @property int $id_author
 * @property int $date_create
 *
 * @property CityReview[] $cityReviews
 * @property User $author
 */
class Review extends \yii\db\ActiveRecord
{
    public $city=[];
    public $other_city;
    public $image_file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'text', 'rating'], 'required'],
            [['title'], 'string'],
            [['text'], 'string'],
            [['rating'], 'integer','max' => 5, 'min' => 1],
            [['image_file'], 'file'],
            [['other_city'], 'string'],
            ['city', 'each', 'rule' => ['string']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Отзыв',
            'rating' => 'Рейтинг',
            'image_file' => 'Изображение',
            'id_author' => 'Id Author',
            'date_create' => 'Date Create',
            'city' => 'Город',
            'other_city' => 'Другой город',
            'img' => 'Изображение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityReviews()
    {
        return $this->hasMany(CityReview::className(), ['id_review' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'id_author']);
    }
    
    public function UploadImage()
    {
        if ($this->image_file = UploadedFile::getInstance($this,'image_file')) {
            $this->image_file->saveAs('img/'.$this->image_file->name);
            $this->img = $this->image_file->name;
        }
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->id_author = Yii::$app->user->id;
                $this->date_create = time();
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->city) {
            foreach ($this->city as $value) {
                if(!CityReview::pairExist($value, $this->id)) {
                    CityReview::createNewPair($value, $this->id);
                    Yii::$app->session->set('city', City::getCityNameById($value));
                }
            }
        } elseif ($this->other_city) {
            if(!City::cityExist($this->other_city)) {
                City::createNew($this->other_city);
            }
            
            $city = City::getCityByName($this->other_city);
            
            if(!CityReview::pairExist($city->id, $this->id)) {
                CityReview::createNewPair($city->id, $this->id);
            }
            
            Yii::$app->session->set('city', $this->other_city);
        } else {
            $all_city = City::find()->all();
            foreach ($all_city as $value) {
                if(!CityReview::pairExist($value->id, $this->id)) {
                    CityReview::createNewPair($value->id, $this->id);
                }
            }
        }
    }
}

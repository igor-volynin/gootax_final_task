<?php

namespace app\modules\user\models;

use Yii;
use app\modules\review\models\Review;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $fio
 * @property string $email
 * @property int $phone
 * @property int $date_create
 * @property string $password
 * @property string $activation_code
 * @property int $is_active
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password_repeat;
    public $verify_code;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'email', 'password', 'password_repeat'], 'required'],
            [['fio', 'email', 'password'], 'string'],
            [['phone'], 'string'],
            [['email'], 'email'],
            [['email'], 'unique', 'message' => 'Такой email уже зарегистрирован.'],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
            [['verify_code'], 'captcha', 'captchaAction' => 'user/default/captcha'],
            [['is_active'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'date_create' => 'Date Create',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'verify_code' => 'Введите код с картинки',
            'activation_code' => 'Activation Code',
            'is_active' => 'Is Active',
        ];
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->date_create = time();
                $this->activation_code = bin2hex(random_bytes(12));
                $hash = Yii::$app->getSecurity()->generatePasswordHash($this->password);
                $this->password = $hash;
            }
            return true;
        } else {
            return false;
        }
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        $mail = Yii::$app->mailer->compose()
            ->setFrom('igorvolsmail@gmail.com')
            ->setTo($this->email)
            ->setSubject('Активация учетной записи')
            ->setHtmlBody('<a href="http://217.29.184.72:8190/register?activate='.$this->activation_code.'">Активировать учетную запись</a>')
            ->send();
    }
    
    public function getReviews()
    {
        return $this->hasMany(Review::className(), ['id_author' => 'id']);
    }
    
    public function findByEmail($email)
    {
        return self::find()->where(['email' => $email])->one();
    }
    
    public function validatePassword($password)
    {
	    return Yii::$app->getSecurity()->validatePassword($password,$this->password) ? true : false;
    }
    
    public static function findIdentity($id)
    {
        return self::findOne($id); 
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
    }
}

<?php

namespace app\modules\city\models;

use Yii;
use app\models\CityReview;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property string $name
 * @property int $date_create
 *
 * @property CityReview[] $cityReviews
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'date_create'], 'required'],
            [['date_create'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'date_create' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityReviews()
    {
        return $this->hasMany(CityReview::className(), ['id_city' => 'id']);
    }
    
    public function getCityList($review = null)
    {
        $list=[];
        if ($review) {
            $city_review = $review->getCityReviews()->with('city')->all();
            foreach ($city_review as $value) {
                $list[$value->city->id] = $value->city->name;
            }
        } else {
            $cities = self::find()->all();
            foreach ($cities as $city) {
                $list[$city->id] = $city->name;
            }
        }
        return $list;
    }
    
    public function getCityByName($name)
    {
        return City::find()->where(['name' => $name])->one();
    }
    
    public function getCityNameById($id)
    {
        return City::find()->where(['id' => $id])->one()->name;
    }
    
    public function cityExist($name)
    {
        if (self::getCityByName($name)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function createNew($name)
    {
        $city = new City;
        $city->name = $name;
        $city->date_create = time();
        $city->save();
    }
}

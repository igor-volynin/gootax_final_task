<?php

use app\models\CityChoose;
use app\modules\city\models\City;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\select2\Select2Bootstrap;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<?php if( Yii::$app->session->hasFlash('send_mail_message') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('send_mail_message'); ?>
    </div>
<?php endif;?>
<?php if( Yii::$app->session->hasFlash('login_is_active') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php echo Yii::$app->session->getFlash('login_is_active'); ?>
    </div>
<?php endif;?>
<?php if (isset($nocity)): ?>
<div class="site-index">
    <div class="jumbotron">
        <p class="lead" id="lead">Отзывов пока нет</p>
        <p><?php if(Yii::$app->user->isGuest){ echo Html::a('Войти', '/login'); echo ' или '; echo Html::a('Зерегистрироваться', '/register'); echo', чтобы оставить отзыв.'; }
                 else { echo Html::a('Добавить отзыв', '/review/create', ['class' => 'btn btn-success']);} ?>
        </p>    
    </div>    
</div>
<?php else: ?>
<div class="site-index">
    <div class="jumbotron">
        <p class="lead" id="lead">Выберите город</p>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->widget(
            Select2Bootstrap::class, 
            [
            'options' => ['class' => "js-example-basic-multiple", 'style' => 'width: 100%'],
            'items' => $cities, // $data should be the same as the items provided to a regular yii2 dropdownlist
            'template' => '{input}'
            ]
        );?>
        <div class="form-group">
            <?= Html::submitButton('Выбрать', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
        </div>    
</div>
<?php endif; ?>